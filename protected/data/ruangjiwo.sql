-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2014 at 09:09 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ruangjiwo`
--

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `time_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `content`, `time_created`) VALUES
(1, 'WELCOME!', '<p>We are a totally integrated creative design agency based in Bandung. We work on all types of specialist creative projects for both traditional and digital marketing campaigns. And we do it for big brand names and busy local businesses alike. So if you are B2B or B2C and based nationally, internationally or just around the corner, please do get in touch. We&#39;ll give you brand new and original creative ideas to bring your campaigns to life and as always will throw in an abundance of friendly and helpful no-nonsense account management along the way. Think of us as your very own helpful, and marketing savvy Squad.</p>\r\n', '2014-11-03 09:57:26'),
(2, 'PHILOSOPHY', '<p>Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy</p>\r\n\r\n<p>&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy&nbsp;Konten philosophy</p>\r\n', '2014-11-03 09:58:05'),
(3, 'VIDEOGRAPHY', '<p>Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;</p>\r\n\r\n<p>Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;Konten Videography&nbsp;</p>\r\n', '2014-11-03 09:58:43'),
(4, 'PHOTOGRAPHY', '<p>Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;</p>\r\n\r\n<p>Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;Konten Photography&nbsp;</p>\r\n', '2014-11-03 09:59:11'),
(5, 'GRAPHIC DESIGN', '<p>Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;</p>\r\n\r\n<p>Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;Konten Graphic Design&nbsp;</p>\r\n', '2014-11-03 09:59:43'),
(6, 'ILUSTRATION', '<p>Konten ilustration&nbsp;Konten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustrationKonten ilustration&nbsp;Konten ilustrationKonten ilustration</p>\r\n\r\n<p>Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;Konten ilustration&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2014-11-03 10:00:17'),
(7, 'WEB DESIGN', '<p>Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;</p>\r\n\r\n<p>Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;Konten Web Design&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2014-11-03 10:00:44'),
(8, 'CONTACT', '<p>PT. ATLAS PRIMACO&nbsp;<br />\r\nJl. Pasirlayung Selatan V/No 9<br />\r\nBandung - 40192<br />\r\nINDONESIA</p>\r\n\r\n<p>Ph. +62 22 70 44 44 35</p>\r\n\r\n<p>+62 819 10 0 91720<br />\r\n+62 813 2251 7335</p>\r\n\r\n<p>ruangjiwo@gmail.com<br />\r\nwww.ruangjiwo-creativeworks.com</p>\r\n', '2014-11-03 10:01:06'),
(9, 'WHY US', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>\r\n', '2014-11-03 10:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `portfolio_category_id` int(11) NOT NULL,
  `client` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `title`, `portfolio_category_id`, `client`, `description`, `image`) VALUES
(1, 'Satria Garuda Bima X', 1, 'Itochu', '<p>Ini adalah isi deskripsi Satria Garuda Bima X</p>\r\n', '1414988023_1414725390_1628219_20140625080940.jpg'),
(2, 'Assassination Classroom', 2, 'A1-Pictures', '<p>Ini isi deskripsi Assassination Classroom</p>\r\n', '1414989931_1414725406_3739996-4025020353-koro_.jpg'),
(3, 'Your Lie in April', 3, 'A-1 Pictures', '<p>Ini isi deskripsi Your Lie in April</p>\r\n', '1414990024_1414725445_magnetic-mode.jpg'),
(4, 'Project 1 Illustration', 4, 'Sunrise', '<p>Ini isi deskripsi Project 1 Illustration</p>\r\n', '1415085935_ff-ix-1.jpg'),
(5, 'Kamen Rider Drive Website', 5, 'TV Asahi', '<p>Ini isi deskripsi Kamen Rider Drive Website</p>\r\n', '1414990107_1414725652_Krdrive.jpg'),
(6, 'Kamen Rider Gaim', 1, 'Ishimori Production', '<p>Ini isi deskripsi Kamen Rider Gaim</p>\r\n', '1414990138_1414725664_SH-Figuarts-Kamen-Rider-Gaim-1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_category`
--

CREATE TABLE IF NOT EXISTS `portfolio_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `portfolio_category`
--

INSERT INTO `portfolio_category` (`id`, `title`) VALUES
(1, 'VIDEOGRAPHY'),
(2, 'PHOTOGRAPHY'),
(3, 'GRAPHIC DESIGN'),
(4, 'ILLUSTRATION'),
(5, 'WEB DESIGN');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_image`
--

CREATE TABLE IF NOT EXISTS `portfolio_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `portfolio_image`
--

INSERT INTO `portfolio_image` (`id`, `portfolio_id`, `title`, `file`) VALUES
(1, 1, 'Test 1', '1414989692_1414725641_Bima-Satria-Garuda-Adik-dari-Kamen-Raider_haibaru650x431.jpg'),
(2, 1, 'Test 2', '1415071752_Untitled-1.jpg'),
(3, 6, 'Kamen Rider Gaim 1', '1415084404_kamen-rider-gaim-1.jpg'),
(4, 6, 'Kamen Rider Gaim 2', '1415084424_kamen-rider-gaim-2.jpg'),
(5, 2, 'Assassination Classroom 1', '1415084940_ass-class-1.jpg'),
(6, 2, 'Assassination Classroom 2', '1415084949_ass-class-2.jpg'),
(7, 3, 'Your Lie In April 1', '1415085427_your-lie-in-april-1.jpg'),
(8, 3, 'Your Lie In April 2', '1415085434_your-lie-in-april-2.jpg'),
(9, 4, 'FF IX 1', '1415086118_ff-ix-1.jpg'),
(10, 4, 'FF IX 2', '1415086125_ff-ix-2.jpg'),
(11, 5, 'Kamen Rider Drive Website 1', '1415088332_drive-website-1.jpg'),
(12, 5, 'Kamen Rider Drive Website 2', '1415088566_drive-website-2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `thumbnail` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `title`) VALUES
(1, 'Project'),
(2, 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE IF NOT EXISTS `slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`id`, `title`, `file`, `time_created`) VALUES
(2, 'Slide 1', '1414569843_1414567885_dummy-image-1.jpg', '2014-10-29 14:31:25'),
(3, 'Slide 2', '1414569852_1414567896_dummy-image-2.jpg', '2014-10-29 14:31:36');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role_id`) VALUES
(1, 'admin', 'admin', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
