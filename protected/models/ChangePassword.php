<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ChangePassword extends CFormModel
{
	public $old_password;
    public $new_password;
    public $repeat_password;
	
	
	public function attributeLabels()
	{
		return array(
			'old_password' => 'Old Password',
			'new_password' => 'New Password',
			'repeat_password' => 'New Password (Repeat)',
		);
	}
	
    //Define the rules for old_password, new_password and repeat_password with changePwd Scenario.
    public function rules()
    {
      return array(
        array('old_password, new_password, repeat_password', 'required', 'on' => 'changePwd'),
        array('old_password', 'findPasswords', 'on' => 'changePwd'),
        array('repeat_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'changePwd','message'=>'{attribute} tidak sesuai'),
      );
    }
 
    //matching the old password with your existing password.
    public function findPasswords($attribute, $params)
    {
        $user = User::model()->findByAttributes(array('username'=>Yii::app()->user->id));
        if ($user->password != $this->old_password)
            $this->addError($attribute, 'Password lama tidak sesuai.');
    }
}