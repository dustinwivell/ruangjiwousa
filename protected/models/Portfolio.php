<?php

/**
 * This is the model class for table "portfolio".
 *
 * The followings are the available columns in table 'portfolio':
 * @property integer $id
 * @property string $title
 * @property integer $portfolio_category_id
 * @property string $client
 * @property string $description
 * @property string $image
 */
class Portfolio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portfolio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, portfolio_category_id, client', 'required'),
			array('portfolio_category_id', 'numerical', 'integerOnly'=>true),
			array('title, client, image', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, portfolio_category_id, client, description, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'PortfolioCategory'=>array(self::BELONGS_TO,'PortfolioCategory','portfolio_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'portfolio_category_id' => 'Portfolio Category',
			'client' => 'Client',
			'description' => 'Description',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('portfolio_category_id',$this->portfolio_category_id);
		$criteria->compare('client',$this->client,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Portfolio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getThumbnail($htmlOptions=array())
	{
		return CHtml::image(Yii::app()->request->baseUrl.'/uploads/portfolio/'.$this->image,'',$htmlOptions);
	}
	
	public function getLatestProject($category_id=null,$limit=3)
	{
		if($category_id!=null)
		{
			$model = Portfolio::model()->findAllByAttributes(array('portfolio_category_id'=>$category_id),array('order'=>'id ASC','limit'=>$limit));
		} else {
			$model = Portfolio::model()->findAll(array('order'=>'id ASC','limit'=>$limit));
		}
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getDataPortfolioImage()
	{
		$model = PortfolioImage::model()->findAllByAttributes(array('portfolio_id'=>$this->id),array('order'=>'id ASC'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getLatestByCategory($category)
	{
		if($category!=null) {
			return Portfolio::model()->findAllByAttributes(array('portfolio_category_id'=>$category),array('order'=>'id DESC'));
		} else {
			return Portfolio::model()->findAll(array('order'=>'id DESC'));			
		}
	}
	
	public function getPortfolioforRead($id)
	{	
		return Portfolio::model()->findByAttributes(array('id'=>$id));
	}
	
	public function getPortfolioImageforRead($id)
	{
		$model = PortfolioImage::model()->findAllByAttributes(array('portfolio_id'=>$id),array('order'=>'id DESC'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getCarouselImage()
	{
		$image = array();
		
		foreach(PortfolioImage::model()->findAllByAttributes(array('portfolio_id'=>$this->id)) as $data)
		{
			array_push($image,array('image'=>Yii::app()->baseUrl.'/uploads/portfolioImage/'.$data->file));
		}
		
		return $image;
	}
}
