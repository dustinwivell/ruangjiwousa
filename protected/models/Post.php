<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id
 * @property integer $post_category_id
 * @property string $title
 * @property string $content
 */
class Post extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('post_category_id', 'numerical', 'integerOnly'=>true),
			array('title, thumbnail', 'length', 'max'=>255),
			array('thumbnail','file','allowEmpty'=>true,'types'=>'jpg,JPG,gif,GIF,png,PNG','maxSize'=>1024*150,'tooLarge'=>'File maksimal 150 KB'),
			array('content, created_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, post_category_id, title, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post_category'=>array(self::BELONGS_TO,'PostCategory','post_category_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post_category_id' => 'Kategori',
			'title' => 'Judul',
			'content' => 'Konten',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post_category_id',$this->post_category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		
		$criteria->order = 'created_time DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getCreatedTime()
	{
		return date('d-m-Y H:i:s',strtotime($this->created_time));
	}
	
	public function getThumbnail($htmlOptions=array())
	{
		return CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$this->thumbnail,'',$htmlOptions);
	}
	
	public function getDataDownload()
	{
		$model = Download::model()->findAllByAttributes(array('model'=>'Post','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getOtherPost()
	{
		$criteria = new CDbCriteria;
		$criteria->limit = 5;
		$criteria->order = 'created_time DESC';
		
		$model = Post::model()->findAll($criteria);
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getLatestPostByCategoryId($category_id=null,$limit=3)
	{
		if($category_id!=null)
		{
			$model = Post::model()->findAllByAttributes(array('post_category_id'=>$category_id),array('order'=>'created_time DESC','limit'=>$limit));
		} else {
			$model = Post::model()->findAll(array('order'=>'created_time DESC','limit'=>$limit));
		}
		
		if($model!==null)
			return $model;
		else
			return false;
	}
}