<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.min.js'); ?>
	<?php Yii::app()->clientScript->registerScript('format-uang','
			$(".format-uang").on("keyup", function() {
				var _this = $(this);
				var value = _this.val().replace(/\.| /g,"");
				_this.val(accounting.formatMoney(value, "", 0, ".", ","))
			});		
			$(".format-uang").trigger("keyup");
	'); ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="row" style="background:#000">
	<div class="col-md-12">&nbsp;</div>
</div>

<div class="row">
	<div class="col-md-12">&nbsp;</div>
</div>

<?php /* $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'type'=>'inverse',
			'items' => array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'items' => array(
						array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
						array('label' => 'View Website','icon'=>'search', 'url' => array('site/index'),'linkOptions'=>array('target'=>'_blank')),
						array('label' => 'Change Password','icon'=>'lock', 'url' => array('user/changePassword')),
						array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
					)
				)
			)
));  */ ?>
	
<div class="containers" id="page">

	<div class="row">
		<div class="col-lg-12">
			<?php echo $content; ?>
		</div>
	</div>


	<div id="footer" style="margin-top:30px;text-align:center;">
		Copyright &copy; 2014 by Ruang Jiwo.
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
