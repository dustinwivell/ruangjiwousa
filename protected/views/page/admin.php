<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('page-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Page</h1>

<?php //$this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'plus white','label'=>'Tambah','url'=>array('page/create'))); ?>

<?php print CHtml::link(Chtml::submitButton('Tambah'),array('page/create')); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'page-grid',
	'dataProvider'=>$model->search(),
	'type'=>'striped bordered',
	'filter'=>$model,
	'columns'=>array(
		'title',
		'time_created',
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
