<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'portfolio-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Kolom dengan tanda <span class="required">*</span> wajib diisi.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'portfolio_category_id',array('widgetOptions'=>array('data'=>CHtml::listData(PortfolioCategory::model()->findAll(),'id','title'),'htmlOptions'=>array('class'=>'span5')))); ?>
	
	<?php echo $form->textFieldGroup($model,'client',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->labelEx($model,'description'); ?>
	<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
			'model'=>$model,
			'attribute'=>'description',
			'filebrowserImageBrowseUrl'=>  Yii::app()->baseUrl.'/vendors/kcfinder/browse.php',
			'filebrowserImageBrowseLinkUrl'=>Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=images',
			'filebrowserFlashBrowseUrl' => Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=Flash',
	)); ?>
	<?php echo $form->error($model,'description'); ?> 
	
	<div>&nbsp;</div>
	<?php print $form->labelEx($model,'image'); ?>
	<?php if(!empty($model->image)) print CHtml::image(Yii::app()->request->baseUrl.'/uploads/portfolio/'.$model->image); ?>
	<?php echo $form->fileField($model,'image',array('class'=>'span5','maxlength'=>255)); ?>
	<?php print $form->error($model,'image'); ?>
	
	<div>&nbsp;</div>
	
<div class="form-actions">
	<?php print CHtml::submitButton('Save'); ?>
	<?php /* $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan',
		)); */ ?>
</div>

<?php $this->endWidget(); ?>
