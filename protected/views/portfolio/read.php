<div class="container" style="width:950px">
	<div class="row">
		<div class="col-lg-12 col-md-12 portofolio-index">
			<h3>PORTOFOLIO</h3>
		</div>
	</div>
	<div class="row portofolio-index">
		<div class="col-lg-1 col-md-1">
			&nbsp;
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-video71"></i>',
				array('portfolio/index','category'=>1), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'VIDEOGRAPHY')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-dslr"></i>',
				array('portfolio/index','category'=>2), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'PHOTOGRAPHY')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-palette5"></i>',
				array('portfolio/index','category'=>3), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'GRAPHIC DESIGN')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-drawing10"></i>',
				array('portfolio/index','category'=>4), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'ILLUSTRATION')) 
			?>
		</div>
		<div class="col-lg-2 col-md-2 category-icon">
			<?php echo CHtml::link('<i class="flaticon-screen52"></i>',
				array('portfolio/index','category'=>5), 
				array('data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'WEB DESIGN')) 
			?>
		</div>
		<div class="col-lg-1 col-md-1">
			&nbsp;
		</div>
	</div>
	<div>&nbsp;</div>
	
	<div class="row">
		<div class="col-lg-8 col-md-8 portofolio-carousel-image">
			<?php $this->widget('booster.widgets.TbCarousel',array(
					'items' => $model->getCarouselImage(),
			)); ?>
		<?php /*
		<div class="index-carousel" style="margin-top:10px">
			<?php foreach(Portfolio::model()->getPortfolioImageforRead($_GET['id']) as $data) { ?>
				<?php print CHtml::image(Yii::app()->baseUrl."/uploads/portfolioImage/".$data->file."","",array("class"=>"","width"=>"600px")); ?>
			<?php } ?>
		</div>
		*/ ?>
		</div>
		<div class="col-lg-4 col-md-4 portofolio-carousel-description">
			<?php $content = Portfolio::model()->getPortfolioforRead($_GET['id']) ?>
			<div class="index-content">
				<h3 style="margin-top:0px;"><?php print $content->title ?></h3>
				<div>&nbsp;</div>
				<div><b>Category</b> : </b><?php print $content->PortfolioCategory->title ?></div>
				<div><b>Client</b> : </b><?php print $content->client ?></div>
				<div>&nbsp;</div>
				<div>
					<?php print $content->description ?>
				</div>
			</div>
		</div>
		<?php /* $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '.index-carousel',
					'config' => array(
						'auto' => array(
							'play' =>true
						),
						'items'=>1,
						'scroll'=>array(
							'fx'=>'crossfade',
							'items'=>1,
							'pauseOnHover'=>false
						),
						'pagination'=>array(
							'items'=>1
						)
					)
		)); */ ?>
	</div>
</div>

<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>