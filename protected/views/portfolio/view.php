<?php
$this->breadcrumbs=array(
	'Portfolios'=>array('index'),
	$model->title,
);
?>

<h1>Lihat Portofolio</h1>

<?php print CHtml::link(Chtml::submitButton('Sunting'),array('portfolio/update','id'=>$model->id)); ?>&nbsp;
<?php print CHtml::link(Chtml::submitButton('Kelola'),array('portfolio/admin')); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'title',
		array(
			'label'=>'Kategori',
			'value'=>$model->PortfolioCategory->title
		),
		'client',
		array(
			'label'=>'Deskripsi',
			'type'=>'raw',
			'value'=>$model->description
		),
		array(
			'label'=>'Image',
			'type'=>'raw',
			'value'=>$model->image == '' ? '' : CHtml::image(Yii::app()->request->baseUrl.'/uploads/portfolio/'.$model->image,'',array('width'=>'600px'))
		),
),
)); ?>

<div>&nbsp;</div>

<h2>Image Portofolio</h2>

<?php print CHtml::link(Chtml::submitButton('Add Image'),array('portfolioImage/create','portfolio_id'=>$model->id)); ?>

<?php /* $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'context'=>'primary',
			'label'=>'Add Image',
			'icon'=>'plus',
			'url'=>array('portfolioImage/create','portfolio_id'=>$model->id,)
)); */ ?>

<div>&nbsp;</div>

<table class="table">
<tr>
	<th>No</th>
	<th>Title</th>
	<th>Image</th>
	<th>Action</th>
</tr>
<?php $i=1; foreach($model->getDataPortfolioImage() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td><?php print CHTml::image(Yii::app()->baseUrl."/uploads/portfolioImage/".$data->file); ?></td>
	<td>
		<a href="<?php print $this->createUrl("portfolioImage/update",array("id"=>$data->id)); ?>"><i class="glyphicon glyphicon-pencil"></i></a>
		<a href="<?php print $this->createUrl("portfolioImage/directDelete",array("id"=>$data->id)); ?>" onclick="return confirm('Yakin akan menghapus file?')"><i class="glyphicon glyphicon-trash"></i></a>
	</td>
</tr>
<?php $i++; } ?>
</table>
