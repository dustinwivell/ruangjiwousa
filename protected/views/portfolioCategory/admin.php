<?php
$this->breadcrumbs=array(
	'Portfolio Categorie'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Kategori Portofolio</h1>

<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Tambah',
			'url'=>array('portfolioCategory/create')
		)); ?>&nbsp;
		
<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'primary',
			'icon'=>'list white',
			'label'=>'Portofolio',
			'url'=>array('portfolio/admin')
		)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'portfolio-category-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'title',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
