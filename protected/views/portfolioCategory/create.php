<?php
$this->breadcrumbs=array(
	'Portfolio Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List PortfolioCategory','url'=>array('index')),
array('label'=>'Manage PortfolioCategory','url'=>array('admin')),
);
?>

<h1>Tambah Kategori Portofolio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>