<?php
$this->breadcrumbs=array(
	'Portfolio Images'=>array('index'),
	'Create',
);
?>

<h1>Tambah Portfolio Image</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>