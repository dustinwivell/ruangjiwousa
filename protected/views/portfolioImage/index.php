<?php
$this->breadcrumbs=array(
	'Portfolio Images',
);

$this->menu=array(
array('label'=>'Create PortfolioImage','url'=>array('create')),
array('label'=>'Manage PortfolioImage','url'=>array('admin')),
);
?>

<h1>Portfolio Images</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
