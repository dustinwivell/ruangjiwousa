<?php
$this->breadcrumbs=array(
	'Portfolio Images'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List PortfolioImage','url'=>array('index')),
	array('label'=>'Create PortfolioImage','url'=>array('create')),
	array('label'=>'View PortfolioImage','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage PortfolioImage','url'=>array('admin')),
	);
	?>

	<h1>Update Portfolio Image</h1>

	<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>