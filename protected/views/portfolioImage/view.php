<?php
$this->breadcrumbs=array(
	'Portfolio Images'=>array('index'),
	$model->id,
);
?>

<h1>Lihat Portfolio Image</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		array(
			'label'=>'Image',
			'type'=>'raw',
			'value'=>$model->file == '' ? '' : CHtml::image(Yii::app()->request->baseUrl.'/uploads/portfolioImage/'.$model->file,'',array('width'=>'600px'))
		),
),
)); ?>
