<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'post-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	
	<?php echo $form->dropDownListGroup($model,'post_category_id',array('widgetOptions'=>array('data'=>CHtml::listData(PostCategory::model()->findAll(),'id','title'),'htmlOptions'=>array('class'=>'span5')))); ?>
	
	<?php echo $form->labelEx($model,'content'); ?>
	<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
			'model'=>$model,
			'attribute'=>'content',
			'filebrowserImageBrowseUrl'=>  Yii::app()->baseUrl.'/vendors/kcfinder/browse.php',
			'filebrowserImageBrowseLinkUrl'=>Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=images',
			'filebrowserFlashBrowseUrl' => Yii::app()->baseUrl.'/vendors/kcfinder/browse.php?type=Flash',
	)); ?>
	<?php echo $form->error($model,'content'); ?> 
	
	<div>&nbsp;</div>
	
	<?php print $form->labelEx($model,'thumbnail'); ?>
	<?php if(!empty($model->thumbnail)) print CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$model->thumbnail); ?>
	<?php echo $form->fileField($model,'thumbnail',array('class'=>'span5','maxlength'=>255)); ?>
	<?php print $form->error($model,'thumbnail'); ?>
	
	<div>&nbsp;</div>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
