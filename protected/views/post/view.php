<?php
$this->breadcrumbs=array(
	'Posts'=>array('index'),
	$model->title,
);

/*
$this->menu=array(
	array('label'=>'List Post','url'=>array('index')),
	array('label'=>'Create Post','url'=>array('create')),
	array('label'=>'Update Post','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Post','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
*/
?>

<h1>Lihat Artikel</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/post/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Baca',
		'context'=>'primary',
		'icon'=>'search white',
		'url'=>array('/post/read','id'=>$model->id),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/post/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/post/admin')
)); ?>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		'post_category_id',
		array(
			'label'=>'Konten',
			'type'=>'raw',
			'value'=>$model->content
		),
		array(
			'label'=>'Thumbnail',
			'type'=>'raw',
			'value'=>$model->thumbnail == '' ? '' : CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$model->thumbnail,'',array('width'=>'600px'))
		),
	),
)); ?>
