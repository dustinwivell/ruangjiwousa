<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'post-category-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
