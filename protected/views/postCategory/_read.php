<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row-fluid">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="tittle-inovasi"><?php echo CHtml::link($data->title, array('/post/read','id'=>$data->id));?></h3>
			<div class='post-read-created-time'>
				<?php print Bantu::getWaktuDibuat($data->created_time); ?>
			</div>
		</div>
	</div>
	<div class="row-fluid">
	<div class="span4">
		<?php if($data->thumbnail!='') print $data->getThumbnail(); else print CHtml::image(Yii::app()->request->baseUrl.'/images/logo-lan.png',''); ?>
	</div>
	<div class="span8">
		<?php echo substr($data->content,0,300).'...';?>
		<?php echo CHtml::link('Selanjutnya', array('/post/read','id'=>$data->id));?></p>
	</div>
	</div>
</div>
<hr>
	