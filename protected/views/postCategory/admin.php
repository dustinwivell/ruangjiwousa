<?php
$this->breadcrumbs=array(
	'Post Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List PostCategory','url'=>array('index')),
	array('label'=>'Tambah Kategori','url'=>array('create')),
	array('label'=>'Kelola Post','url'=>array('post/admin')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('post-category-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Kategori Post</h1>

<?php $this->widget('booster.widgets.TbButtonGroup', array(
    'context'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
		array('label'=>'Pilihan Menu','items'=>array(
			array('label'=>'Tambah Kategori','icon'=>'plus','url'=>array('/postCategory/create')),
			array('label'=>'Kelola Post','icon'=>'th-list','url'=>array('/post/admin')),
		)),
    ),
)); ?>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'post-category-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'columns'=>array(
			'title',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
