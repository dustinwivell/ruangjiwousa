<?php
$this->breadcrumbs=array(
	'Post Categories'=>array('index'),
	$model->title,
);

$this->menu=array(
	//array('label'=>'List PostCategory','url'=>array('index')),
	array('label'=>'Tambah Kategori','url'=>array('create')),
	array('label'=>'Sunting Kategori','url'=>array('update','id'=>$model->id)),
	array('label'=>'Kelola Kategori','url'=>array('admin')),
	array('label'=>'Hapus Kategori','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kategori','url'=>array('admin')),
);
?>

<h1>Lihat Kategori Artikel</h1>


<?php /* $this->widget('booster.widgets.TbButtonGroup', array(
    'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
		array('label'=>'Pilihan Menu','items'=>array(
			array('label'=>'Sunting Kategori','icon'=>'pencil','url'=>array('/postCategory/update','id'=>$model->id)),
			array('label'=>'Tambah Kategori','icon'=>'plus','url'=>array('/postCategory/create')),
			array('label'=>'Kelola Kategori','icon'=>'th-list','url'=>array('/post/admin')),
		)),
    ),
)); 

<div>&nbsp;</div>

*/ ?>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
		),
)); ?>
