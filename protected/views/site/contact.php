<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);
?>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'action'=>array('site/contact'),
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textField($model,'name',array('class'=>'form-control input-sm','placeholder'=>'Name')); ?>
		<?php echo $form->error($model,'name'); ?>
		
		<div>&nbsp;</div>
		
		<?php echo $form->textField($model,'email',array('class'=>'form-control input-sm','placeholder'=>'Email')); ?>
		<?php echo $form->error($model,'email'); ?>
		
		<div>&nbsp;</div>

		<?php echo $form->textArea($model,'body',array('class'=>'form-control input-sm','placeholder'=>'Comment', 'rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
		
		<div>&nbsp;</div>
		
		<?php echo CHtml::submitButton('Submit',array('class'=>'submitButton')); ?>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?>