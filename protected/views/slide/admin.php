<?php
$this->breadcrumbs=array(
	'Slides'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List Slide','url'=>array('index')),
	//array('label'=>'Create Slide','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('slide-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Slide</h1>

<?php print CHtml::link(Chtml::submitButton('Tambah'),array('slide/create')); ?>

<?php //$this->widget('booster.widgets.TbButton',array('buttonType'=>'link','context'=>'primary','icon'=>'plus white','label'=>'Tambah','url'=>array('slide/create'))); ?>


<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'slide-grid',
	'dataProvider'=>$model->search(),
	'type'=>'striped bordered',
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'class'=>'CDataColumn',
			'header'=>'file',
			'type'=>'raw',
			'value'=>'CHtml::image(Yii::app()->request->baseUrl."/uploads/slide/".$data->file,"",array("width"=>"400px"))'
		),
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
