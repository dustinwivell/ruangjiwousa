<div class="form">
 
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'change-password-form',
            'enableClientValidation' => true,
            'htmlOptions' => array('class' => 'well'),
            'clientOptions' => array(
            'validateOnSubmit' => true,
            ),
     ));
?>
	<?php echo $form->passwordFieldGroup($model,'old_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'new_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'repeat_password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?><div>&nbsp;</div>
	
	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit', 
		'context' => 'primary', 
		'label' => 'Change password',
		'icon' => 'floppy-disk'
		)); ?>
	</div>
<?php $this->endWidget(); ?>
</div>