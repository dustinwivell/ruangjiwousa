<?php
$this->breadcrumbs=array(
	'User'=>array('admin'),
	$model->username,
);
?>

<h1>Lihat User</h1>

<?php print CHtml::link(Chtml::submitButton('Sunting'),array('user/update','id'=>$model->id)); ?>&nbsp;
<?php print CHtml::link(Chtml::submitButton('Kelola'),array('user/admin')); ?>


<?php /*
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'pencil white','label'=>'Sunting User','url'=>array('user/update','id'=>$model->id))); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'plus white','label'=>'Tambah User','url'=>array('user/create'))); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'list white','label'=>'Kelola User','url'=>array('user/admin'))); ?>
*/ ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'username',
			array(
				'label'=>'Role',
				'value'=>$model->Role->nama
			),
		),
)); ?>
